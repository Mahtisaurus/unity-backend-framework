const mongoose = require('mongoose').set('debug', true);
const Schema = mongoose.Schema;

const Player = Schema({
    "player_tag": {
        type: String,
        unique: true,
    },
    "status": {
        type: Boolean,
        default: false
    },
    "failed_flips": {
        type: Number,
    },
    "succeeded_flips": {
        type: Number,
    },
    "losses": {
        type: Number,
    },
    "wins": {
        type: Number,
    },
    "playtime_in_hours": {
        type: Number,
    },
    "currency": {
        type: Number,
    },
    "inventory": {
        "outfits": [
            {
                "id": {
                    type: Number,
                },
                "name": {
                    type: String,
                },
                "timestamp": String
            }
        ]
    },
    "current_outfit":
    {
        "id": {
            type: Number,
        },
        "timestamp": {
            type: String,
        },
    }
}, { collection: 'player' }, { __v: false });

module.exports = mongoose.model('Players', Player);