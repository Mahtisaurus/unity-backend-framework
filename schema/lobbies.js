const mongoose = require('mongoose').set('debug', true);
const Schema = mongoose.Schema;

const Lobbies = Schema({
    lobby: [
        {
            type: String,
            required: true
        }
    ]
}, { collection: 'lobbies' }, { __v: false });

module.exports = mongoose.model('lobbies', Lobbies);