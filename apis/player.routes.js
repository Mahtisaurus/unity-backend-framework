require('dotenv');
const express = require('express');
const router = express.Router();
const playerCtrl = require('../controller/player.controller');


router.post('/create', playerCtrl.createplayer);
router.put('/status', playerCtrl.updateStatus);
router.get('/status/:status', playerCtrl.getByStatus);
router.post('/updateCurrency', playerCtrl.updateCurrency);
router.post('/joinLobby', playerCtrl.joinLobby);
router.get('/lobbyPlayers/:id', playerCtrl.getLobbyPlayers);
router.delete('/removeFromLobby/:id', playerCtrl.removeFromLobby);
router.get('/:id', playerCtrl.getPlayer);
router.put('/:id', playerCtrl.updatePlayer);

module.exports = router;