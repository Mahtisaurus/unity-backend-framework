const resp = require('../resp');

const Player = require('../schema/player');
const Lobbies = require("../schema/lobbies");

module.exports = class {
    static createplayer(req, res) {
        try {
            const player = new Player(req.body);
            player.save((err, data) => {
                console.log(data);
                if (err) return resp.success(res, null, err.message);
                // const token = generateToken(data);
                return resp.success(res, { player: data });
            });
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async updateStatus(req, res) {
        try {
            const _id = req.body.id;
            if (!_id)
                return resp.error(res, 'Provide id of player');

            await Player.findOneAndUpdate({ _id }, { $set: { status: req.body.status } })

            return resp.success(res, `Status is set to ${req.body.status}`);
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async getByStatus(req, res) {
        try {
            const status = req.params.status;
            const result = await Player.find({ status });
            return resp.success(res, result);
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async updateCurrency(req, res) {
        try {
            const _id = req.body.id;
            if (!_id)
                return resp.error(res, 'Provide id of player');

            await Player.findOneAndUpdate({ _id }, { $set: { currency: req.body.currency } })
            return resp.success(res, `Curreny is increased by ${req.body.currency}`);
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async joinLobby(req, res) {
        try {
            const _id = req.body.id;
            if (!_id)
                return resp.error(res, 'Provide id of player');

            const currentLobbies = await Lobbies.find({});

            const lobby = currentLobbies.filter(lobby => lobby.lobby.length < 4);

            if (lobby.length) {
                // there is space in lobby, add in it
                console.log('there is space in lobby, add in it');
                await Lobbies.findOneAndUpdate({ _id:lobby[0]._id }, { $push: { lobby: _id } });
            } else {
                // no lobby found, creating new one
                console.log('no lobby found, creating new one');
                let newLobby = new Lobbies();
                newLobby.lobby.push(_id);
                newLobby.save();
            }
            await Player.findOneAndUpdate(
                { _id },
                { $set: { status: true } }
            );
            return resp.success(res, `You have been added into the lobby`);
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async getLobbyPlayers(req, res) {
        try {
            const _id = req.body.id;
            if (!_id)
                return resp.error(res, 'Provide id of player');

            const currentLobby = await Lobbies.findOne({ lobby: { $in: [_id] } });
            const players = [];

            for (let _id of currentLobby.lobby) {
                console.log(_id);
                let player = await Player.findOne({ _id });
                players.push(player);
            }
            return resp.success(res, players);
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async removeFromLobby(req, res) {
        try {
            const _id = req.params.id;
            if (!_id)
                return resp.error(res, 'Provide id of player');

            await Lobbies.findOneAndUpdate(
                { _id },
                { $pull: { lobby: _id } }
            );

            await Player.findOneAndUpdate(
                { _id },
                { $set: { status: false } }
            );

            return resp.success(res, 'You have been removed from lobby');
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async getPlayer(req, res) {
        try {
            const _id = req.params.id;
            if (!_id)
                return resp.error(res, 'Provide id of player');

            const player = await Player.findOne({ _id });

            return resp.success(res, player);
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }

    static async updatePlayer(req, res) {
        try {
            const _id = req.params.id;
            if (!_id)
                return resp.error(res, 'Provide id of player');

            const player = await Player.update(req.body);

            return resp.success(res, 'Player updated successfully');
        } catch (err) {
            console.error(err);
            return resp.success(res, err.message);
        }
    }
}
